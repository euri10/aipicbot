Downlaod your u2net models:

- `u2net.pth <https://drive.google.com/file/d/1ao1ovG1Qtx4b7EoskHXmi2E9rp5CHLcZ/view>`_
- `u2netp.pth <https://drive.google.com/file/d/1rbSTGKAE-MTxBYHd-51l2hMOQPT_7EPy/view>`_
- `u2net_human_seg.pth <https://drive.google.com/file/d/1-Yg0cxgrNhHP-016FPdp902BR-kSsA4P/view>`_

Put them accessible in a directory that is defined in your config, the key is `u2net_models_location`.

Send your bot a command and see the result:

.. image:: docs/example_readme.png
  :width: 400
  :alt: Example

The original image being:

.. image:: docs/original_readme.jpg
  :width: 400
  :alt: Original

If you're using docker, you'll need a maubot image that has the required dependencies, basically the ones defined here in the pyproject.toml, a working dockerfile can be found here:

`debian.dockerfile <https://raw.githubusercontent.com/euri10/maubot/debian/debian.dockerfile>`_
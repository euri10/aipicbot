import io
import logging
import time
from enum import Enum
from pathlib import Path
from typing import Any, Optional, Tuple, Type

from maubot import MessageEvent, Plugin
from maubot.handlers import command
from mautrix.types import ImageInfo, MediaMessageEventContent, MessageType
from mautrix.util.config import BaseProxyConfig, ConfigUpdateHelper

from .u2net_process import remove_bg
from .utils import BGEnum

try:
    from PIL import Image
except ImportError:
    Image = None


logger = logging.getLogger("aipic")


class Config(BaseProxyConfig):
    def do_update(self, helper: ConfigUpdateHelper) -> None:
        helper.copy("u2net_models_location")


class ImageNotFoundError(Exception):
    pass


class EnumArgument(command.Argument):
    def __init__(
        self, name: str, label: str = None, *, required: bool = False, enum: Type[Enum]
    ) -> None:
        super().__init__(name, label, required=required, pass_raw=False)
        self.enum = enum

    def match(self, val: str, **kwargs: Any) -> Tuple[str, Any]:
        logger.info("MATCH")
        part = val.split(" ")[0].lower()
        try:
            res = self.enum(part)
        except Exception:
            logger.error(
                f"Invalid enum, choose in {list(str(v.value) for v in self.enum)}"
            )
            raise ValueError()
        return val[len(part) :], res


class AIPicBot(Plugin):
    @classmethod
    def get_config_class(cls) -> Type[BaseProxyConfig]:
        return Config

    async def start(self) -> None:
        await super().start()
        logger.info("Starting...")
        self.config.load_and_update()
        self.model_path = Path(self.config["u2net_models_location"])
        logger.debug(self.model_path)
        logger.debug(self.model_path.exists())

    @command.new(name="aipic", help="Run aipic bot", require_subcommand=True)
    async def aipic(self, evt: MessageEvent) -> None:
        pass

    @aipic.subcommand(name="bg", help="Remove image background")
    @EnumArgument(
        name="model_name",
        label=f"Model name: {list(str(v.value) for v in BGEnum)}",
        required=True,
        enum=BGEnum,
    )
    @command.argument(
        name="img_url",
        label="Image URL",
        required=True,
        matches=r"(^https?.*\.(gif|jpe?g|bmp|png)$)",
    )
    async def u2net(self, evt: MessageEvent, model_name: BGEnum, img_url: str) -> None:
        t0 = time.time()
        logger.info("AIPIC")
        logger.debug(evt)
        logger.debug(model_name)
        logger.debug(img_url)

        logger.info(f"Transform with {model_name.name} model")
        image_data = await self._get_image(img_url[0])
        if image_data:
            transformed_data = remove_bg(
                data=image_data,
                model_location=self.model_path,
                model_name=model_name,
            )
            if transformed_data:
                uri = await self.client.upload_media(
                    io.BytesIO(transformed_data), mime_type="image/png"
                )
        logger.debug(f"URI: {uri}")
        t1 = time.time()
        time_spent = t1 - t0
        await self.client.send_text(
            evt.room_id,
            text=f"f{img_url}",
            html=f"Transformed using: <strong>{model_name.name}</strong> model in {time_spent:.2f}s",
        )
        content = MediaMessageEventContent(
            url=uri,
            msgtype=MessageType.IMAGE,
            info=ImageInfo(
                mimetype="image/png",
            ),
        )
        await self.client.send_message(evt.room_id, content)

    async def _get_image(self, img_url: str) -> Optional[bytes]:
        resp = await self.http.get(img_url)
        logger.debug(resp)
        if resp.status == 200:
            data = await resp.read()
            return data
        else:
            logger.error("Cant get that image")
            raise ImageNotFoundError

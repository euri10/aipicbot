import logging
from io import BytesIO
from pathlib import Path
from typing import Dict, Optional, Union

import numpy as np
import torch
from PIL import Image
from torch import FloatTensor, Tensor
from torchvision.transforms import transforms

from .data_loader import RescaleT, ToTensorLab
from .u2net import U2NET, U2NETP
from .utils import BGEnum

logger = logging.getLogger("aipic")


def load_model(
    model_location: Path, model_name: BGEnum
) -> Optional[Union[U2NET, U2NETP]]:
    net: Optional[Union[U2NET, U2NETP]] = None
    logger.debug(f"Loading model: {model_name}")
    if model_name is BGEnum.U2NET or BGEnum.U2NET_HUMAN_SEG:
        net = U2NET(3, 1)
    elif model_name is BGEnum.U2NETP:
        net = U2NETP(3, 1)
    else:
        logger.error("NO MODEL FOUND")
    path = model_location / f"{model_name.name.lower()}.pth"
    if not path.exists():
        logger.error("Model does not exist")
        return None
    else:
        assert net
        if torch.cuda.is_available():
            net.load_state_dict(torch.load(path))
            net.to(torch.device("cuda"))
        else:
            net.load_state_dict(
                torch.load(
                    path,
                    map_location="cpu",
                )
            )
        net.eval()
        return net


def norm_pred(d: Tensor) -> Tensor:
    ma = torch.max(d)
    mi = torch.min(d)
    dn = (d - mi) / (ma - mi)

    return dn


def preprocess(image: np.ndarray) -> Dict[str, Tensor]:
    label_3 = np.zeros(image.shape)
    label = np.zeros(label_3.shape[0:2])

    if 3 == len(label_3.shape):
        label = label_3[:, :, 0]
    elif 2 == len(label_3.shape):
        label = label_3

    if 3 == len(image.shape) and 2 == len(label.shape):
        label = label[:, :, np.newaxis]
    elif 2 == len(image.shape) and 2 == len(label.shape):
        image = image[:, :, np.newaxis]
        label = label[:, :, np.newaxis]

    transform = transforms.Compose([RescaleT(320), ToTensorLab(flag=0)])
    sample = transform({"imidx": np.array([0]), "image": image, "label": label})

    return sample


def predict(net: Union[U2NET, U2NETP], item: np.ndarray) -> Image:

    sample = preprocess(item)

    with torch.no_grad():

        if torch.cuda.is_available():
            inputs_test = FloatTensor(sample["image"].unsqueeze(0).cuda().float())
        else:
            inputs_test = torch.FloatTensor(sample["image"].unsqueeze(0).float())

        d1, d2, d3, d4, d5, d6, d7 = net(inputs_test)

        pred = d1[:, 0, :, :]
        predict = norm_pred(pred)

        predict = predict.squeeze()
        predict_np = predict.cpu().detach().numpy()
        img = Image.fromarray(predict_np * 255).convert("RGB")

        del d1, d2, d3, d4, d5, d6, d7, pred, predict, predict_np, inputs_test, sample

        return img


def remove_bg(
    data: bytes,
    model_location: Path,
    model_name: BGEnum,
) -> Optional[memoryview]:
    model = load_model(model_location, model_name)
    assert model
    img = Image.open(BytesIO(data)).convert("RGB")
    mask = predict(model, np.array(img)).convert("L")
    empty = Image.new("RGBA", (img.size), 0)
    cutout = Image.composite(img, empty, mask.resize(img.size, Image.LANCZOS))
    bio = BytesIO()
    cutout.save(bio, "PNG")
    return bio.getbuffer()

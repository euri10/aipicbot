.DEFAULT_GOAL := help

PY_SRC := aipicbot
PY_SRC_TEST := tests
#PY_DOCS := docs/examples

.PHONY: check
check: check-black check-flake8 check-isort check-mypy

.PHONY: check-black
check-black:  ## Check if code is formatted nicely using black.
	black -v --check --diff $(PY_SRC)
	#black -v --check --diff $(PY_SRC) $(PY_DOCS)

.PHONY: check-flake8
check-flake8:  ## Check for general warnings in code using flake8.
	flake8 --exclude=.venv $(PY_SRC)
	#flake8 --exclude=.venv $(PY_SRC) $(PY_DOCS)

.PHONY: check-mypy
check-mypy:  ## Check for general warnings in code using mypy.
	mypy $(PY_SRC)
	#mypy $(PY_SRC) $(PY_DOCS)

.PHONY: check-isort
check-isort:  ## Check for general warnings in code using isort.
	isort --check --diff $(PY_SRC)
	#isort --check --diff $(PY_SRC) $(PY_DOCS)

.PHONY: clean
clean: ## Delete temporary files.
	@rm -rf $(PY_SRC)/build 2>/dev/null
	@rm -rf $(PY_SRC)/dist 2>/dev/null
	@rm -rf $(PY_SRC)/.coverage* 2>/dev/null
	@rm -rf $(PY_SRC)/.pytest_cache 2>/dev/null
	@rm -rf .mypy_cache 2>/dev/null
	@rm -rf pip-wheel-metadata 2>/dev/null

.PHONY: help
help:  ## Print this help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort

.PHONY: lint-black
lint-black:  ## Lint the code using black.
	black $(PY_SRC)
	#black $(PY_SRC) $(PY_DOCS)

.PHONY: format
format: ## use black and isort to format code
	black $(PY_SRC)
	isort $(PY_SRC)
	#black $(PY_SRC) $(PY_DOCS)
	#isort $(PY_SRC) $(PY_DOCS)

.PHONY: test
test: clean  ## Run the tests using coverage with pytest module.
	cd $(PY_SRC_TEST)
	coverage run

.PHONY: report
report: ## coverage report
	cd $(PY_SRC)
	coverage report

.PHONY: html
html: ## coverage report
	cd $(PY_SRC)
	coverage html

.PHONY: maubot
maubot: ## test maubot plugin
	cd $(PY_SRC)
	cd ~/mydocker/synapse && docker-compose exec maubot mbc build /plugindev --upload

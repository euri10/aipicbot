import asyncio
from pathlib import Path

import aiohttp
import pytest

from aipicbot import u2net_process
from aipicbot import AIPicBot


@pytest.mark.asyncio
async def test_get_image():
    loop = asyncio.get_event_loop()
    async with aiohttp.ClientSession() as session:
        plugin = AIPicBot(client=None, loop=loop, http=session, instance_id=None, log=None, config=None, database=None, webapp=None, webapp_url=None )
        image_data = await plugin._get_image(img_url="https://upload.wikimedia.org/wikipedia/commons/d/d9/Chameleon02.jpg")
        transformed_data = u2net_process.remove_bg(
            data=image_data,
            model_location=Path("/home/lotso/mydocker/synapse/maubot/models"),

            model_name="u2net",
        )
        print(transformed_data)